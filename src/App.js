import React, {Component} from 'react';
import List from './components/List.js';
import axios from 'axios';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      ideasCount : 0,
      ideas : [],
      mounted: false,
    }
  }
  componentWillMount(){
    let app = this;
    // IF ENDPOINT EXISTS
    // axios.get('http://demo8093953.mockable.io/ideas')
    axios.get('http://sample-rest.io/ideas')
      .then(function(response){
        app.setState({
          ideasCount : response.data.length,
          ideas : response.data,
          mounted: true
        });
        console.log(app.state.ideas);
      })
      .catch(function(error){
        console.log(error);
         // IF NOT
        if(localStorage.getItem('data')){
          var dataPull = localStorage.getItem('data');
          console.log(dataPull);
          var data = [];
          if(dataPull){
              data = JSON.parse(dataPull);
          }
          app.setState({
              ideasCount: data.length,
              ideas : data,
              mounted: true,
          });
          console.log(app.state.ideas);
        }else{
          app.setState({
            mounted:true,
          })
        }
      })
  }
  componentDidMount(){
    console.log(this.state.ideas);
  }
  render(){
    return (
      <div className="App">
        <header className="App-header">
            <h1 className="text-centered">My Ideas</h1>
        </header>
        {this.state.mounted ? <List dataset={this.state.ideas} dataCount={this.state.ideasCount} /> : "" }
      </div>
    );
  }
}

export default App;
