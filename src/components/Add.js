import React, { Component } from 'react';

class Add extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){

  }
    render() {
      return (
        <div className="add-button" onClick={this.props.addFunc}>
            <p>+ Add New Idea</p>
        </div>
      );
    }
  }
  
  export default Add;