import React, { Component } from 'react';
import Idea from './Idea.js';
import Add from './Add.js';
import Sort from './Sort.js';
import axios from 'axios';
import uuid from 'uuidv4';
import moment from 'moment/moment.js';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
        ideasCount : this.props.ideasCount,
        ideas : this.props.dataset,
        activeSort : 'created_date'
    }

    this.addIdea = () =>{
        // Rest Sample
        const app = this;
        let addURL = 'http://sample-rest.io/ideas/new';
        axios.get(addURL)
        .then(function(response){
            
        })
        .catch(function(error){
            console.log('Error message: ' + error);
            let ideasNew = app.state.ideas;
            let created = moment().format();
            let uid = uuid();
            ideasNew.unshift({id:uid,title:"",body:"",created_date:created});
            app.setState({
                ideasCount : app.state.ideasCount + 1,
                ideas : ideasNew,
            });
            localStorage.setItem('data',JSON.stringify(app.state.ideas));
        })
      }

    this.removeIdea = (e) =>{
        const app = this;
        let id = e.target.getAttribute('value');
        let deleteURL = "http://sample-rest.io/idea/delete";
        let dataset = app.state.ideas;
        axios.delete(deleteURL, {params: { id: id }})
        .then(function(response){
            console.log('deleted successfully');
        })
        .catch(function(error){
            console.log('Error Message: ', error);
            let removeData = dataset.findIndex(function(item){
                return item.id === id;
            });
            if(removeData !== -1){
                dataset.splice(removeData,1);
            }else{
                console.log('none!');
            }
            app.setState({
                ideas: dataset,
            });
            localStorage.setItem('data',JSON.stringify(app.state.ideas));
        })
    }

    this.updateIdea = (e) =>{
        let element = e.target.getAttribute('class');
        let id = e.target.getAttribute('target');
        let value = e.target.value;
        let dataset = this.state.ideas;
        let updateURL = "http://sample-rest.io/idea/update/" + id;
        let updateData = dataset.findIndex(function(item){
            return item.id === id;
        });
        if(updateData !== -1){
            if(element === 'title'){
                if(dataset[updateData].title !== value){
                    axios.post(updateURL,{
                        id : id,
                        title : value
                    }).then(function(response){
                        
                    }).catch(function(error){
                        console.log(error);
                    })
                    dataset[updateData].title = value;
                    this.showToast();
                }
            }else if(element === 'body'){
                if(dataset[updateData].body !== value){
                    axios.post(updateURL,{
                        body : value
                    }).then(function(response){
                        
                    }).catch(function(error){
                        console.log(error);
                    })
                    dataset[updateData].body = value;
                    this.showToast();
                }
            }
        }else{
            console.log('none!');
        }
        this.setState({
            ideas: dataset,
        });
        localStorage.setItem('data',JSON.stringify(this.state.ideas));
    }

    this.showToast = () =>{
        document.getElementById('toast').classList.add('show');
        setTimeout(function(){
            document.getElementById('toast').classList.remove('show');
        },3000);
    }

    this.sortItems = (property) =>{
        var sortOrder = 1;

        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
    
        return function (a,b) {
            if(sortOrder == -1){
                return b[property].localeCompare(a[property]);
            }else{
                return a[property].localeCompare(b[property]);
            }        
        }
    }
    this.sortIdeas = (option) =>{
        let optionVal = option.currentTarget.value;
        if(optionVal === 'title'){
            this.setState({
                activeSort: optionVal
            });
        }else{
            this.setState({
                activeSort: optionVal
            });
        }
    }
  }
    componentDidMount(){
        this.setState({
            ideasCount: this.props.ideasCount,
            ideas: this.props.dataset
        })
    }
    render() {
        const ideas = [];
        const toSort = [];
        const deleteFunc = this.removeIdea;
        const updateFunc = this.updateIdea;

        this.state.ideas.forEach(function(d){
            toSort.push(d);
        });
        if(this.state.activeSort === 'title'){
            toSort.sort(this.sortItems('title'));
            toSort.forEach(function(d){
                ideas.push(<Idea key={d.id} uid={d.id} title={d.title} body={d.body} date={moment(d.created_date).format('MMMM D YYYY')} deleteFunc={deleteFunc} updateFunc={updateFunc} />);
            })
        }else{
            toSort.sort(function(x,y){
                return x.created_date - y.created_date;
            });
            toSort.forEach(function(d){
                ideas.push(<Idea key={d.id} uid={d.id} title={d.title} body={d.body} date={moment(d.created_date).format('MMMM D YYYY')} deleteFunc={deleteFunc} updateFunc={updateFunc} />);
            })
        }
      return (
        <div className="idea-list">
            <Sort sortFunc={this.sortIdeas}/>
            <div id="toast">Changes Saved</div>
            {ideas}
            <Add addFunc={this.addIdea} />
        </div>
      );
    }
  }
  
  export default List;