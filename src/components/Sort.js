import React, { Component } from 'react';

class Sort extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount(){

  }
    render() {
      return (
        <div className="sort">
            <span>Sort by: </span>
            <select onChange={this.props.sortFunc}>
                <option value="created-date">Created Date</option>
                <option value="title">Title</option>
            </select>
        </div>
      );
    }
  }
  
  export default Sort;