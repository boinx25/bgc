import React, { Component } from 'react';

class Idea extends Component {
  constructor(props) {
    super(props);
    this.bodyText = React.createRef();
    this.title = React.createRef();
    this.state = {
      showCounter:false,
      charCount: 15,
    }
    this.countChar = () =>{
      const maxChar = 140;
      let charCount = this.bodyText.current.value.length;
      
      if(maxChar - charCount > 15){
        this.setState({
          showCounter:false,
        });
      }else{
        this.setState({
          showCounter:true,
          charCount: maxChar - charCount
        });
      }
    }


    this.updateField = () => {
      this.setState({
        showCounter:false
      });
    }

    this.textBlur = (e) => {
      this.updateField();
      this.props.updateFunc(e);
    }
   }
   componentDidMount(){
    if(this.title.current.value.length === 0){
      console.log('novalue!');
      this.title.current.focus();
    }else{
      console.log('value!');
    }  
   }
    render() {
      return (
        <div className="idea">
            <span className='date'>{this.props.date}</span>
            <input ref={this.title} className="title" name="title" spellCheck="false" defaultValue={this.props.title} onBlur={this.props.updateFunc} target={this.props.uid} />
            <textarea className="body" maxLength="140" ref={this.bodyText} name="body" spellCheck="false" defaultValue={this.props.body} onFocus={this.countChar} onChange={this.countChar} onBlur={this.textBlur} target={this.props.uid} />
            <div className="delete"><span onClick={this.props.deleteFunc} value={this.props.uid}>X</span></div>
            {this.state.showCounter ? <span className="char-count">{this.state.charCount} {this.state.charCount === 1 ? "character remaining" : "characters remaining"}</span> : ""}
        </div>
      );
    }
  }
  
  export default Idea;